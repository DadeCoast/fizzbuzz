//David Camargo
//FizzBuzz, Kenzie Assignment

//Running Variable to maxValue of 50, testing for multiples of 3, 5, and testing if
//Even or odd, as well as a prime number

//calling function
function FizzBuzz() 
{


    //maxValue
    let maxValue = 50;

    //For Loop W/ assinged componets
    for (var i = 1; i < maxValue; i++)
    {
        if (i % 3 === 0 && i % 5 === 0){
            console.log("FizzBuzz");
        }
        else if (i % 5 === 0) {
            console.log("Buzz");
        }
        else if (i % 3 === 0) {
            console.log("Fizz");
        }
        else {
            console.log(i);
        }
    }
}
FizzBuzz();